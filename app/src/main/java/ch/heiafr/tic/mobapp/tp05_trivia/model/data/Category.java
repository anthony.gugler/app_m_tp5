package ch.heiafr.tic.mobapp.tp05_trivia.model.data;

/**
 * Data class to hold data about a category
 */
public class Category {

    //=== Attributes

    private final int id;             // category id in the trivia api
    private final String title;       // category name


    //=== Constructors

    public Category(int id, String title) {
        this.id = id;
        this.title = title;
    }


    //=== Getters

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }
}
