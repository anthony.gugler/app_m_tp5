package ch.heiafr.tic.mobapp.tp05_trivia.model.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Category;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;

public class ApiOpenTrivia {
    private Context context;

    public ApiOpenTrivia(Context context) {
        this.context = context;
    }

    public void getCategory() {
        RequestQueue mQueue = Volley.newRequestQueue(context);
        String url = "https://opentdb.com/api_category.php";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("trivia_categories");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject cat = jsonArray.getJSONObject(i);
                                Category c = new Category(
                                        Integer.valueOf(cat.getString("id")),
                                        cat.getString("name"));
                                System.out.println(c);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        // Add the request to the RequestQueue.
        mQueue.add(request);
    }

    public void getQuestion(int id, int nbQuestion) {
        RequestQueue mQueue = Volley.newRequestQueue(context);
        String url = "https://opentdb.com/api.php?amount=" + nbQuestion +
                "&category=" + id + "&type=multiple";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject questions = jsonArray.getJSONObject(i);
                                ArrayList<String> answers = new ArrayList<>();
                                JSONArray lesAnswers = questions.getJSONArray("incorrect_answers");
                                for (int j = 0; j < lesAnswers.length(); j++) {
                                    answers.add(lesAnswers.getString(i));
                                }

                                Question q = new Question(
                                        questions.getString("category"),
                                        questions.getString("type"),
                                        questions.getString("difficulty"),
                                        questions.getString("question"),
                                        questions.getString("correct_answer"),
                                        answers
                                );
                                System.out.println(q);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        // Add the request to the RequestQueue.
        mQueue.add(request);
    }
}
