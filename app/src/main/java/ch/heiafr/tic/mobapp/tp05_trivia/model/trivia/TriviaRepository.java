package ch.heiafr.tic.mobapp.tp05_trivia.model.trivia;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

/**
 * Repository handling the whole trivia logic and holding data
 */
public class TriviaRepository {

    //=== Constants

    public static final int MODE_HISTORY = 0;               // mode when history
    public static final int MODE_GAME = 1;                  // mode when game
    public static final int DEFAULT_CORRECT_ANSWER = 0;     // default correct answer position
    public static final int QUESTION_NUMBER = 3;           // number of questions
    public static final float MEDAL_GOLD = 0.8f;            // gold medal rating
    public static final float MEDAL_SILVER = 0.6f;          // silver medal rating
    public static final float MEDAL_BRONZE = 0.4f;          // bronze medal rating
    public static final int ANSWER_NUMBER = 4;              // number of answers per question
    public static final int QUESTION_INTERVAL = 1500;       // time interval before next question
    public static final String QUESTION_TYPE = "multiple";  // question type
    public static final String EASY = "easy";               // easy difficulty text
    public static final String MEDIUM = "medium";           // medium difficulty text


    //=== Attributes

    private final Application application;                  // application context

    private MutableLiveData<TriviaMedal> medal;             // result medal image
    private MutableLiveData<String> textMedal;              // result medal text
    private MutableLiveData<Integer> corrects;              // correctly answered questions

    // TODO: To implement


    //=== Singleton

    private static TriviaRepository INSTANCE;       // class unique instance

    // Create the unique instance of the TriviaRepository class.
    private TriviaRepository(Application application) {
        this.application = application;
        // TODO: To implement
    }

    // Get the unique instance of the TriviaRepository class.
    public static TriviaRepository getInstance(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new TriviaRepository(application);
        }
        return INSTANCE;
    }


    //=== Getters and setters

    // Gets the number of correctly answered questions.
    public LiveData<Integer> getCorrects() {
        return corrects;
    }

    // Get the medal image.
    public LiveData<TriviaMedal> getMedal() {
        return medal;
    }

    // Get the text associated to the medal.
    public LiveData<String> getTextMedal() {
        return textMedal;
    }

    // TODO: To implement


    //=== Public methods

    public void determineMedal() {
        // TODO: To implement
    }

    // TODO: To implement


    //=== Private methods

    // TODO: To implement


}
